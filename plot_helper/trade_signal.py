import pandas as pd
import matplotlib.pyplot as plt

def plot_trade_singal(data,price_label='Adj Close'):
    # Generate moving averages
    data.sort_index(inplace=True)  # sort the dataframe based on index

    data['Mavg5'] = data[price_label].rolling(window=5).mean()
    data['Mavg20'] = data[price_label].rolling(window=20).mean()

    # Save moving averages for the day before
    prev_short_mavg = data['Mavg5'].shift(1)
    prev_long_mavg = data['Mavg20'].shift(1)

    # Select buying and selling signals: where moving averages cross
    buys = data.ix[(data['Mavg5'] <= data['Mavg20']) & (prev_short_mavg >= prev_long_mavg)]
    sells = data.ix[(data['Mavg5'] >= data['Mavg20']) & (prev_short_mavg <= prev_long_mavg)]

    # The label parameter is useful for the legend
    plt.plot(data.index, data[price_label], label='EOS/ETH price')
    plt.plot(data.index, data['Mavg5'], label='5-day moving average')
    plt.plot(data.index, data['Mavg20'], label='20-day moving average')

    plt.plot(buys.index, data.ix[buys.index][price_label], '^', markersize=10, color='g')
    plt.plot(sells.index, data.ix[sells.index][price_label], 'v', markersize=10, color='r')

    plt.ylabel('EOS/ETH price')
    plt.xlabel('Date')
    plt.title("EOS Trading Signal")
    plt.legend(loc=0)
    plt.show()

if __name__ == "__main__":
    data = pd.DataFrame.from_csv(path='../csv/EOSETH_daily.csv', sep=',')
    plot_trade_singal(data)