import requests
from datetime import datetime

url = 'https://www.binance.com/api/v1/klines?symbol=EOSETH&interval=1d'

def write_data_to_csv(url,csv_file_name):
    r = requests.get(url)
    results = r.json()
    csv_file = open(csv_file_name,"w+")
    csv_file.write("Date,Open,High,Low,Close,Adj Close,Volume\n")
    for result in results:
        start_timestamp,open_p,high,low,close,volume = result[0],result[1],result[2],result[3],result[4],result[5]
        timestamp_f = float(str(start_timestamp)[:10])
        timestamp = datetime.fromtimestamp(timestamp_f)
        csv_file.write("{},{},{},{},{},{},{}\n".format(timestamp,open_p,high,low,close,close,volume))
    csv_file.close()
write_data_to_csv(url,"EOSETH_daily.csv")
