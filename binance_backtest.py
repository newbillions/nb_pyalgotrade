from datetime import datetime as dt
import pandas as pd
from stockstats import StockDataFrame as Sdf
from binance.enums import *
import time

def get_kline_data_from_binance(client,symbol="EOSETH",interval=KLINE_INTERVAL_1DAY):
    klines = client.get_klines(symbol=symbol, interval=interval)
    d = {"Date":[],"Open":[],"High":[],"Low":[],"Close":[],"Volume":[]}

    for kline in klines:
        [open_time, open_price, high_price, low_price, close_price, volume, close_time,
         quote_asset_volume, number_of_trades,
         taker_buy_base_asset_volume, taker_buy_quote_asset_volume, _] = kline
        timestamp_f = float(open_time/1000.0)
        timestamp = dt.fromtimestamp(timestamp_f)
        d["Date"].append(timestamp)
        d["Open"].append(open_price)
        d["High"].append(high_price)
        d["Low"].append(low_price)
        d["Close"].append(close_price)
        d["Volume"].append(volume)

    df = pd.DataFrame(data=d)
    df['Open'] = df['Open'].astype('float64')
    df['High'] = df['High'].astype('float64')
    df['Low'] = df['Low'].astype('float64')
    df['Close'] = df['Close'].astype('float64')
    df['Volume'] = df['Volume'].astype('float64')
    stat = Sdf.retype(df)
    return (stat,df)

# Deprecated: as we can't get all trade history from binance
def get_trade_history_from_binance_helper(client,symbol,timestamp,fromId=0):
    print("fromID "+str(fromId))
    history = client.get_my_trades(symbol=symbol, timestamp=timestamp,fromId=fromId)
    df = pd.DataFrame(history, columns=['id', 'orderId','price','qty','commission','commissionAsset','time','isBuyer','isMaker','isBestMatch'])
    df['time'] = pd.to_datetime(df['time'],unit="ms")
    df['time'] = df['time'].apply(lambda x: x.replace(microsecond=0))
    # df.set_index('id', inplace=True)

    # print("\t\t{}".format(df.iloc[[0, -1]]))
    if len(history) != 0:
        return df,max(history, key=lambda x: x['id'])['id'] + 1
    else:
        return df,fromId+1
def f(row):
    if row['isBuyer'] == True:
        return 'BUY'
    else:
        return 'SELL'

# Deprecated: as we can't get all trade history from binance
def get_trade_history_from_binance(client,symbol="EOSETH",timestamp=time.time()*1000):
    df,min_id = get_trade_history_from_binance_helper(client,symbol,timestamp)

    prev_min_id = -1
    loop = 0
    while(loop < 10):
        loop += 1
        prev_min_id = min_id
        tmp_df, min_id = get_trade_history_from_binance_helper(client, symbol, timestamp,fromId=prev_min_id)
        df = df.append(tmp_df)
        # df.set_index('id', inplace=True)

    # df = df.drop_duplicates()
    df.price = df.price.astype(float)
    df.qty = df.qty.astype(float)
    df.commission = df.commission.astype(float)
    df['Total'] = df.price * df.qty
    df['Market'] = symbol
    df = df.rename(columns= {'qty':'Amount'})
    df=df.rename(columns = {'price':'Price'})
    df['Type'] = df.apply(f, axis=1)
    df = df.rename(columns= {'commission':'Fee'})
    df.set_index('time', inplace=True)
    # df=df.rename(columns = {'price':'Price'})
    return df

# Given a filtered_data, this function will split it into 2 df. non-wash & wash df
def split_nonwash_and_wash(filtered_data):
    # Build helper_dict
    # Key: timestamp
    # Value: row
    helper_dict = dict()
    for index, row in filtered_data.iterrows():
        if index not in helper_dict:
            helper_dict[index] = [row]
        else:
            helper_dict[index].append(row)

    # Store rows that are not washing txs
    non_washing_results = []
    # Store rows that are washing tx pairs.
    washing_results = []

    for timestamp in helper_dict:
        arrs = helper_dict[timestamp]
        if len(arrs) == 1:
            # If there is only 1 tx in this timestamp, it won't be washing tx pairs.
            non_washing_results += arrs
            continue

        # Otherwise, we build washing_dict to store all the avaliable tx type for a given amount
        # Key: total_token in tx
        # Value: [tx type (BUY or SELL)]
        washing_dict = dict()
        for row in arrs:
            if row.Total not in washing_dict:
                washing_dict[row.Total] = [row.Type]
            else:
                washing_dict[row.Total].append(row.Type)

        for row in arrs:
            types = washing_dict[row.Total]
            if 'SELL' in types and 'BUY' in types:
                # If both SELL & BUY have appeared in types, it is washing pairs.
                washing_results.append(row)
            else:
                # Else it is not. e.g. (BUY,BUY,BUY)
                non_washing_results.append(row)
    # Sanity check
    assert((len(non_washing_results) + len(washing_results)) == len(filtered_data))
    non_washing_results_df = None
    washing_results_df = None

    if len(non_washing_results) != 0:
        non_washing_results_df = pd.DataFrame.from_items([(s.name, s) for s in non_washing_results]).T
    if len(washing_results) != 0:
        washing_results_df = pd.DataFrame.from_items([(s.name, s) for s in washing_results]).T

    return (non_washing_results_df,washing_results_df)

def total_fee_cal_helper(df,type='BUY'):
    return df[df.Type == type]['Fee'].sum()

def calculate_from_df(filtered_data,target_symbol,base_symbol,is_from_api=False):
    non_washing_results_df,washing_results_df = split_nonwash_and_wash(filtered_data)

    #Buy stat
    buy_data = filtered_data[filtered_data.Type == "BUY"]
    total_buy_fee = total_fee_cal_helper(buy_data)
    token_total_buy = int(buy_data['Amount'].sum())
    paid_token_total = int(buy_data['Total'].sum())

    non_washing_buy_df = non_washing_results_df[non_washing_results_df.Type == "BUY"]
    total_non_washing_buy_fee = total_fee_cal_helper(non_washing_buy_df)
    non_washing_token_total_buy = int(non_washing_buy_df['Amount'].sum())
    non_washing_paid_token_total = int(non_washing_buy_df['Total'].sum())

    if washing_results_df is not None:
        washing_buy_df = washing_results_df[washing_results_df.Type == "BUY"]
        total_washing_buy_fee = total_fee_cal_helper(washing_buy_df)
        washing_token_total_buy = int(washing_buy_df['Amount'].sum())
        washing_paid_token_total = int(washing_buy_df['Total'].sum())

    non_washing_avg_buy_price = non_washing_paid_token_total/non_washing_token_total_buy

    print("\n------------BUY------------\n")
    print("total buy: {} {}".format(token_total_buy,target_symbol))
    print("\t non-washing-total buy: {} {}".format(non_washing_token_total_buy,target_symbol))
    if washing_results_df is not None: print("\t washing-total buy: {} {}".format(washing_token_total_buy,target_symbol))
    print("\ntotal cost: {} {}".format(paid_token_total,base_symbol))
    print("\t non-washing-total cost: {} {}".format(non_washing_paid_token_total,base_symbol))
    if washing_results_df is not None: print("\t washing-total cost: {} {}".format(washing_paid_token_total,base_symbol))
    print("\navg non-washing BUY price: {} {}/{}".format(non_washing_avg_buy_price,target_symbol,base_symbol))
    print("\nTotal fee: {} {}".format(total_buy_fee,target_symbol))
    print("\t non-washing-fee: {} {}".format(total_non_washing_buy_fee,target_symbol))
    if washing_results_df is not None: print("\t washing-fee: {} {}".format(total_washing_buy_fee,target_symbol))

    #Sell stat
    sell_data = filtered_data[filtered_data.Type == "SELL"]
    total_sell_fee = total_fee_cal_helper(sell_data,type='SELL')
    token_total_sell = int(sell_data['Amount'].sum())
    get_token_total = int(sell_data['Total'].sum())

    non_washing_sell_df = non_washing_results_df[non_washing_results_df.Type == "SELL"]
    total_non_washing_sell_fee = total_fee_cal_helper(non_washing_sell_df,type='SELL')
    non_washing_token_total_sell = int(non_washing_sell_df['Amount'].sum())
    non_washing_get_token_total = int(non_washing_sell_df['Total'].sum())

    if washing_results_df is not None:
        washing_sell_df = washing_results_df[washing_results_df.Type == "SELL"]
        total_washing_sell_fee = total_fee_cal_helper(washing_sell_df,type='SELL')
        washing_token_total_sell = int(washing_sell_df['Amount'].sum())
        washing_get_token_total = int(washing_sell_df['Total'].sum())

    if non_washing_token_total_sell != 0:
        non_washing_avg_sell_price = non_washing_get_token_total/non_washing_token_total_sell
        print("\n------------SELL------------\n")
        print("total sell: {} {}".format(token_total_sell,target_symbol))
        print("\t non-washing-total sell: {} {}".format(non_washing_token_total_sell,target_symbol))
        if washing_results_df is not None: print("\t washing-total sell: {} {}".format(washing_token_total_sell,target_symbol))
        print("\ntotal GET: {} {}".format(get_token_total,base_symbol))
        print("\t non-washing-total GET: {} {}".format(non_washing_get_token_total,base_symbol))
        if washing_results_df is not None: print("\t washing-total GET: {} {}".format(washing_get_token_total,base_symbol))
        print("\navg non-washing SELL price: {} {}/{}".format(non_washing_avg_sell_price,target_symbol,base_symbol))
        print("\nTotal fee: {} {}".format(total_sell_fee,base_symbol))
        print("\t non-washing-fee: {} {}".format(total_non_washing_sell_fee,base_symbol))
        if washing_results_df is not None: print("\t washing-fee: {} {}".format(total_washing_sell_fee,base_symbol))


    p_l_percentage = (non_washing_get_token_total - non_washing_paid_token_total)/(non_washing_paid_token_total) * 100
    print("\n------------Key Metrics------------\n")
    print("P&L%: {}%".format(p_l_percentage))

def get_trade_history_from_downloaded_csv(symbol='EOSETH',target_symbol='EOS',base_symbol='ETH'):
    print("\n\n\t\tSymbol "+symbol+"\n\n")
    all_data = pd.DataFrame.from_csv(path='csv/TradeHistory.csv')
    filtered_data = all_data[all_data.Market == symbol]
    # filtered_data.loc[len(filtered_data)]= ['RDNETH','BUY',0.00219063233,684733.8,1500,0.0000,'ETH']
    calculate_from_df(filtered_data,target_symbol,base_symbol)

if __name__ == "__main__":
    import sys
    from binance.client import Client
    from plot_helper.candle_stick import *
    from plot_helper.trade_signal import plot_trade_singal
    encry_pass = sys.argv[1]
    client = Client(encry_pass, "txts/binance")

    # stat, df = get_kline_data_from_binance(client)
    # build_from_df(stat, df)
    # plot_trade_singal(df,price_label="close")

    get_trade_history_from_downloaded_csv(symbol='LINKETH',target_symbol='LINK',base_symbol='ETH')
    # get_trade_history_from_downloaded_csv(symbol='RDNBTC',target_symbol='RDN',base_symbol='BTC')
    df = get_trade_history_from_binance(client,symbol='LINKETH')
    # df.to_csv("EOSETH.csv")
    # df.loc[len(df)]= [123,0.00219063233,684733.8,0.0,'ETH','2017-11-11 23:46:22',True,False,True,'BUY',0.0,684733.8,1500]
    calculate_from_df(df,target_symbol='LINK',base_symbol='ETH')
